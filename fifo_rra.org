#+TITLE: Round Robin Arbiter - Verification document
#+OPTIONS: toc:nil H:4
* Abstract
This document describes the high-level and low-level architecture of the RRA verification as discussed in class as our example. It includes:
 * Block assumptions and feautures list
 * Verification environment structure
 * Verification phases
 * Interfaces with other environments
* Overview
The RRA verification environment is used to simulate data flow of various packets (MODEM, VOIP, TV, INTERNET) through the RRA system discussed in class.
* Requirements and assumptions
 * No packets are lost
 * Minimum packet size is 64 bytes
* Features
The RRA verification environment can:
 * Drive various data payloads (VOIP, TV, INTERNET, MODEM)
 * Drive the payloads at rates up to 2 Gbps
 * Receive payloads at rates up to 1 Gbps (at the output of the DUV)
 * Do scoreboard checking
 * Do statistical scoreboard checking 
 * Simulate reset and clock for the DUV
* Environment architecture
** Standalone environment structure
In this mode the environment gets data from the drivers via the BFM.

#+ATTR_LaTeX: width=10cm
#+CAPTION: Verification environment block diagram
[[file:verification_environment.png]]

** General environment description
*** Interfaces
The environment has 3 interfaces.
 - Main payload interface :: Main source of payload on input.
 - Clock/Reset Generator :: Drives reset and main clock to the DUV and environment.
 - Output interface :: Drives the various monitors at the output of the DUV.
*** Environment basic modules
# Shameless copy/paste from original document
The environment will be build according to the eRM principles, each I/F is connected to three Specman based components:
 - BFM ::  Bus functional machine, this module will actually connect between the Specman environment and the verilog code (DUT).
 - Monitor :: Observes the I/F and collects information regarding the data transfer through the specific I/F.
 - Driver :: When an I/F is Active (Data should be injected through it) the driver is the module which generate the data or sequence of data and send it to the DUT (using the BFM).
*** Reference models
There is one reference model in our verification environment for this design.
 - Protocol reference model :: This reference model will simulate the operation of the DUT on the packet level. It's main responsibility is sending the payload packets that were injected to the DUT onto the scoreboard with the correct header, thereby creating a list for it's type.
*** Simulation vs. DUT Checking
The verification environment has several points where the comparison between the reference models and DUT is being done:
 - Packet scoreboard :: The output of the protocol reference model is being compared with the actual DUT’s output that was reassembled in the output packet collector. This is done by comparing each packet according to its type with the list of packets sent to the scoreboard by the protocol reference model.
 - Statistical scoreboard :: The output of the reference model is compared statistically to the amount of packets of each type passing through the design. This is done so as to verify the correctness of the RRA.
*** Verification phases
There are three verification phases in our example design:
 1. Protocol phase\\
    During this phase the protocol's implementation will be tested at the level of a payload packet.
 2. Performance phase\\
    At this stage the performance of the design will be tested at the level of packet streams.
 4. Full chip
** Full chip environment structure
This structure is unimplemented for our example design.
** TODO Units detailed description
*** Main payload I/F
**** TODO Driver
The driver is responsible for on-the-fly generation of random packets.
These packets are set built into a struct of /payload/ and /header/.
*TODO: Deal with "marking" the packages payload so as to retrace the packets type.*

#+BEGIN_SRC c++
void * driver()
{
  struct payload_t {
    int size;
    unsigned int data;
  }

  struct packet_t {
    unsigned int header;
    payload_t payload;
  }

  packet_t * packet;
  packet.header = rand(0..3);
  packet.payload.size = rand(64..2047);
  packet.payload.data = rand(0x00..0xFFFFFFFFF);

  return packet;
}
#+END_SRC
**** BFM
The Bus Functional Model takes at its input the randomized packets created by the driver.
It randomizes the rate at which data is sent per data block.
At its output it drives the input lines of our example module.
#+BEGIN_SRC verilog
module bfm (input packet, clk,
	    output data, valid, start_s, end_s);

   defparam MAXRATE = 2147483648; // rate = 1             : 2 Gbps
                                  // rate = 2'147'483'648 : 1 bps

   int 		   rate = rand(1..MAXRATE); 
   reg 		   data_s <= packet.header && packet.payload;
                   // Obviously some padding code is needed here

   always @(posedge clk)
     if (valid == 1)
       valid <= 0;
   ... // same for start_s and end_s
  
   begin
      for (i = 0 ; i <= data_s.length() ;  i=i+8)
	@(posedge clk*rate)
	  begin
	     if (i == 0)    // announce packet start
	       start_s <= 1;
	     if (i == data_s.length()) // announce packet finish
	       end_s <= 1;
	     valid <= 1;
	     data <= data_s[MSB-i..MSB-i+8];
	     rate = rand(1..2147483648);
	  end
   end

endmodule // bfm
#+END_SRC
**** Rate monitor
The rate monitors function is to monitor the current rate at which data enters and leaves the system. We can *not* use the bus's clock to measure the rate, this has to be done by counting the amount of times valid has changed.

The monitors are generic for input and output, they are instantiated with /MAXTHROUGHPUT/ variable that controls at which point the system should announce an error has occured.

#+BEGIN_SRC c++
#define MAXTHROUGHPUT = 2147483648;

int rate_monitor (void * signals)
{
  int data_packets_throughput = 0;

  while(1)
    {
      alarm(1); // send SIGALARM to process after 1 sec
      if(SIGALARM)
	break;
      for ( int i = 0 ; i < MAXTHROUGHPUT+500 ; i++ )
	{
	  if (signals.valid == 1)
	    {
	      wait(signals.valid == 0);
	      data_packets_throughput++;
	      if (data_packets_throughput >= MAXTHROUGHPUT)
		raise_error("Rate too high");
	    }
	}
    }
  return data_packets_throughput;
}
#+END_SRC
**** TODO Packet monitor
The packet monitor's objective is to reassemble the packet created from the signals and forward it to the reference model.
*TODO: write the implementation of the source code*
#+BEGIN_SRC c++
packet_t * packet_monitor(signals_t * signals)
{
  // 1. Discover start
  // 2. 1st valid: write 2 bits to header rest to payload
  // 3. While no "end" repeat 4.
  // 4. For every valid write data to payload
  // 5. Return packet structure
}
#+END_SRC
*** Output I/F
**** Rate monitor
The rate monitor at the output I/F is exactly the same as the one at the input I/F.
It differs only at the fact the /MAXTHROUGHPUT/ definiton which will be scaled back to 1 Gb.
**** TODO Packet monitor
The packet monitor's objective on the output is to reassemble the packet created from the signals, including the discovery of it's type and to forward it to the scoreboard.
*TODO: write the implementation of the source code*
#+BEGIN_SRC c++
packet_t * packet_monitor(signals_t * signals)
{
  // 1. Discover start
  // 2. While no "end" repeat 3.
  // 3. For every valid write data to payload.
  // 4. Discover type from "marking" in payload
  // 5. Return packet structure
}
#+END_SRC
*** TODO Clock/Reset Generator
*** TODO Reference models
**** Protocol reference model
*** TODO Scoreboards
**** Packet scoreboard
**** Statistical scoreboard
** TODO Other verification environment interfaces
** TODO Open issues
